# Hardware Benchmark
硬件跑分部分分为CPU, GPU, Memory, Disk四部分组成

## CPU跑分
CPU跑分程序基于CoreMark的源码，修改为dll方式调用，并修改对应分数算法

CoreMak : https://www.eembc.org/coremark/

GITHUB : https://github.com/eembc/coremark

```
核心公式 score = thread * iterations / time_in_secs(total_time)
CPU跑分计算规则：
score = 1thread * 0.5 + 4thread * 0.42 + 64thread * 0.08
```

BUILD：本程序基于MinGW编译（gcc > 8.1）
```
cd ./cpu/
make
```

备注：基准测试LOG（I7-8700）
```
2K performance run parameters for coremark.	(Run type)
CoreMark Size    	: 666					(Buffer size)
Total ticks			: 25875					(platform dependent value)
Total time (secs) 	: 25.875000				(actual time in seconds)
Iterations/Sec 		: 3864.734300			(Performance value to report)
Iterations			: 100000				(number of iterations used)
Compiler version	: GCC3.4.4				(Compiler and version)
```

## GPU跑分算法
GPU跑分分为两部分：OPENCL + OPENGL

其中OPENCL可以静默跑分，但是只能进行浮点数跑分测试，无法测试3D游戏性能

### OPENCL算法
OPENCL跑分可以代表机器的4K渲染能力，浮点数计算能力，图片/视频渲染能力等

原理是使用GPU进行浮点数计算，并发多个线程进行线程处理能力的测试

跑分算法基础是来源于GITHUB开源项目 ： Mixbench https://github.com/ekondis/mixbench

核心代码：
``` 
cl_event event;
const size_t local_work_size[1] = {64};
const size_t global_work_size[1] = {10240};
clock_t begin = clock();
clEnqueueNDRangeKernel(cmd_queue, kernel, 1, NULL, global_work_size, local_work_size, 0, NULL, &event);
clWaitForEvents(1, &event);
clock_t end = clock();
double runtime = (double)(end - begin);

OPENCL Code:

for(int i=0; i < CORENUMBER; i++){
    for(int j=i; j < 100; j++){
        for(int k=0; k < 100; k++){
            out[num] = (a[num] + b[num]) - (a[num] - b[num]) * a[num] + b[num] * (a[num] - b[num]) / a[num];
        }
    }
}

fast test:
SET CORENUMBER = 256
score = (1500000 * 100) / total_time / 100
```

BUILD：本程序基于MinGW编译（gcc > 8.1, OPENCL）
```
cd ./opencl/
make
```

备注 ：基准测试Result (NVidia RTX-2070 GPU): 
```
mixbench/read-only (v0.03-2-gbccfd71)
------------------------ Device specifications ------------------------
Device:              GeForce RTX 2070
CUDA driver version: 10.20
GPU clock rate:      1620 MHz
Memory clock rate:   3500 MHz
Memory bus width:    256 bits
WarpSize:            32
L2 cache size:       4096 KB
Total global mem:    7979 MB
ECC enabled:         No
Compute Capability:  7.5
Total SPs:           2304 (36 MPs x 64 SPs/MP)
Compute throughput:  7464.96 GFlops (theoretical single precision FMAs)
Memory bandwidth:    448.06 GB/sec
-----------------------------------------------------------------------
Total GPU memory 8366784512, free 7941521408
Buffer size:          256MB
Trade-off type:       compute with global memory (block strided)
Elements per thread:  8
Thread fusion degree: 4
----------------------------------------------------------------------------- CSV data -----------------------------------------------------------------------------
Experiment ID, Single Precision ops,,,,              Double precision ops,,,,              Half precision ops,,,,                Integer operations,,, 
Compute iters, Flops/byte, ex.time,  GFLOPS, GB/sec, Flops/byte, ex.time,  GFLOPS, GB/sec, Flops/byte, ex.time,  GFLOPS, GB/sec, Iops/byte, ex.time,   GIOPS, GB/sec
            0,      0.250,    0.32,  104.42, 417.68,      0.125,    0.63,   53.04, 424.35,      0.500,    0.32,  211.41, 422.81,     0.250,    0.32,  105.58, 422.30
            1,      0.750,    0.32,  316.34, 421.79,      0.375,    0.63,  158.69, 423.18,      1.500,    0.32,  634.22, 422.81,     0.750,    0.32,  317.30, 423.07
            2,      1.250,    0.32,  528.46, 422.77,      0.625,    0.78,  215.91, 345.45,      2.500,    0.32, 1055.97, 422.39,     1.250,    0.32,  528.57, 422.86
            3,      1.750,    0.32,  738.81, 422.17,      0.875,    1.08,  218.17, 249.34,      3.500,    0.32, 1478.95, 422.56,     1.750,    0.32,  740.59, 423.20
            4,      2.250,    0.32,  951.33, 422.81,      1.125,    1.38,  219.57, 195.17,      4.500,    0.32, 1902.66, 422.81,     2.250,    0.32,  950.66, 422.51
            5,      2.750,    0.32, 1162.74, 422.81,      1.375,    1.67,  220.38, 160.28,      5.500,    0.32, 2328.52, 423.37,     2.750,    0.32, 1162.74, 422.81
            6,      3.250,    0.32, 1374.56, 422.94,      1.625,    1.97,  220.99, 135.99,      6.500,    0.32, 2756.62, 424.10,     3.250,    0.32, 1375.81, 423.32
            7,      3.750,    0.32, 1592.45, 424.65,      1.875,    2.27,  221.38, 118.07,      7.500,    0.32, 3169.50, 422.60,     3.750,    0.32, 1585.55, 422.81
            8,      4.250,    0.32, 1796.95, 422.81,      2.125,    2.57,  221.71, 104.33,      8.500,    0.32, 3587.76, 422.09,     4.250,    0.37, 1545.63, 363.68
            9,      4.750,    0.32, 2006.34, 422.39,      2.375,    2.87,  221.85,  93.41,      9.500,    0.32, 3995.38, 420.57,     4.750,    0.32, 1998.29, 420.69
           10,      5.250,    0.32, 2209.52, 420.86,      2.625,    3.17,  222.02,  84.58,     10.500,    0.32, 4439.54, 422.81,     5.250,    0.32, 2220.44, 422.94
           11,      5.750,    0.32, 2434.12, 423.32,      2.875,    3.47,  222.17,  77.28,     11.500,    0.32, 4855.01, 422.17,     5.750,    0.32, 2426.77, 422.05
           12,      6.250,    0.32, 2638.06, 422.09,      3.125,    3.78,  222.18,  71.10,     12.500,    0.32, 5227.20, 418.18,     6.250,    0.38, 2202.15, 352.34
           13,      6.750,    0.32, 2841.95, 421.03,      3.375,    4.08,  222.30,  65.87,     13.500,    0.32, 5712.58, 423.15,     6.750,    0.32, 2850.54, 422.30
           14,      7.250,    0.32, 3065.39, 422.81,      3.625,    4.37,  222.45,  61.36,     14.500,    0.32, 6135.74, 423.15,     7.250,    0.32, 3065.08, 422.77
           15,      7.750,    0.33, 3143.40, 405.60,      3.875,    4.67,  222.57,  57.44,     15.500,    0.32, 6546.34, 422.34,     7.750,    0.32, 3268.89, 421.79
           16,      8.250,    0.32, 3482.59, 422.13,      4.125,    4.98,  222.57,  53.96,     16.500,    0.32, 6957.48, 421.67,     8.250,    0.39, 2803.68, 339.84
           17,      8.750,    0.32, 3693.66, 422.13,      4.375,    5.28,  222.53,  50.86,     17.500,    0.32, 7396.24, 422.64,     8.750,    0.32, 3694.77, 422.26
           18,      9.250,    0.32, 3901.58, 421.79,      4.625,    5.58,  222.58,  48.12,     18.500,    0.32, 7786.72, 420.90,     9.250,    0.32, 3897.66, 421.37
           20,     10.250,    0.32, 4312.53, 420.73,      5.125,    6.18,  222.66,  43.45,     20.500,    0.32, 8640.66, 421.50,    10.250,    0.41, 3374.54, 329.22
           22,     11.250,    0.32, 4729.94, 420.44,      5.625,    6.78,  222.74,  39.60,     22.500,    0.32, 9452.31, 420.10,    11.250,    0.32, 4734.21, 420.82
           24,     12.250,    0.32, 5148.83, 420.31,      6.125,    7.36,  223.51,  36.49,     24.500,    0.32,10346.40, 422.30,    12.250,    0.42, 3900.12, 318.38
           28,     14.250,    0.32, 6009.94, 421.75,      7.125,    8.53,  224.23,  31.47,     28.500,    0.32,11975.32, 420.19,    14.250,    0.44, 4368.11, 306.53
           32,     16.250,    0.32, 6795.36, 418.18,      8.125,    9.72,  224.31,  27.61,     32.500,    0.32,13605.64, 418.64,    16.250,    0.45, 4797.12, 295.21
           40,     20.250,    0.34, 7899.43, 390.10,     10.125,   12.11,  224.50,  22.17,     40.500,    0.33,16371.37, 404.23,    20.250,    0.50, 5464.85, 269.87
           48,     24.250,    0.41, 8029.04, 331.09,     12.125,   14.49,  224.58,  18.52,     48.500,    0.40,16468.89, 339.56,    24.250,    0.54, 5986.22, 246.85
           56,     28.250,    0.47, 8114.58, 287.24,     14.125,   16.88,  224.65,  15.90,     56.500,    0.46,16443.12, 291.03,    28.250,    0.60, 6342.42, 224.51
           64,     32.250,    0.53, 8154.47, 252.85,     16.125,   19.26,  224.72,  13.94,     64.500,    0.52,16536.22, 256.38,    32.250,    0.66, 6591.93, 204.40
           80,     40.250,    0.66, 8242.80, 204.79,     20.125,   24.03,  224.79,  11.17,     80.500,    0.65,16644.88, 206.77,    40.250,    0.78, 6909.54, 171.67
           96,     48.250,    0.78, 8321.35, 172.46,     24.125,   28.80,  224.85,   9.32,     96.500,    0.78,16685.23, 172.90,    48.250,    0.91, 7108.62, 147.33
          128,     64.250,    1.03, 8337.22, 129.76,     32.125,   38.34,  224.91,   7.00,    128.500,    1.03,16775.65, 130.55,    64.250,    1.18, 7295.18, 113.54
          192,     96.250,    1.54, 8414.49,  87.42,     48.125,   57.42,  224.97,   4.67,    192.500,    1.53,16847.93,  87.52,    96.250,    1.74, 7431.64,  77.21
          256,    128.250,    2.06, 8362.01,  65.20,     64.125,   76.50,  225.02,   3.51,    256.500,    2.06,16693.65,  65.08,   128.250,    2.30, 7477.75,  58.31
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
```

![](docs/rtx2070-sp-roofline.png)

### OPENGL跑分
OPENGL跑分需要运行模拟游戏的图像界面，使用OPENGL模拟光追效果，并计算当前机器的FPS值

OPENGL跑分可以代表机器的3D图像性能，游戏渲染能力等指标

跑分代码基于GITHUB开源项目 ： https://github.com/engilas/raytracing-opengl

```
设置屏幕宽高为固定大小 ： 1366 * 768
score = FPS / total_time * (1366 * 768) / 10000
```

![](docs/opengl_1.png)

BUILD：本程序基于MinGW编译（gcc > 8.1, OPENGL）
```
cd ./gpu/
make
```

## Memory 跑分
基础原理是使用多线程进行内存的读写测试，并根据读写总时间进行内存的打分

核心代码：
```
/**    seq read & write   **/
src = (char*)malloc(1024*1024*1024*sizeof(char));
dest = (char*)malloc(1024*1024*1024*sizeof(char));
long int no_blocks = 1024*1024*1024*sizeof(char)/(b_size*no_threads);
for(j=0; j<no_blocks; j++){
    memcpy(dest, src, b_size);
    src = src + b_size;
    dest = dest + b_size;
}
/**    seq write   **/
src = (char*)malloc(1024*1024*1024*sizeof(char));
for(k=0;k<no_blocks;k++){
    memset(src, 'A', b_size);
	src = src + b_size;
}
/**    random write   **/
long int no_blocks_new = 1024*1024*1024*sizeof(char)/(b_size*no_threads);
for(k=0;k<no_blocks_new;k++){
    long int rand_temp = rand()%no_blocks_new;
    rand_loc = src1 + (rand_temp)*b_size;
    memset(rand_loc, 'A', b_size);
    incr += 1;
}

EACH in (SIZE, thread)： 
    [
        (8K, 1), (8M, 1), (80M, 1), (800M, 1),
        (8K, 2), (8M, 2), (80M, 2), (800M, 2),
        (8K, 4), (8M, 4), (80M, 4), (800M, 4),
        (8K, 8), (8M, 8), (80M, 8), (800M, 8),
    ]

```

BUILD：本程序基于MinGW编译（gcc > 8.1）
```
cd ./memory/
make
```

### Disk跑分
基础原理是使用单线程进行系统Temp文件(一般在Windows系统盘)进行读写操作

核心代码：
```
buffer = bytearray(size)
for i in range(loop):
    f = open(file, 'wb')
    f.write(buffer)
    f.close()
    f = open(file, 'r')
    f.read(size)
    f.close()

EACH in (SIZE, LOOP): 
    [
        (1024, 2000), 
        (1024 * 1024, 500), 
        (1024 * 1024 * 10, 50), 
        (1024 * 1024 * 100, 10),
    ]

score = 2000 / (score / 10)
```

# 后台跑分
后台跑分数据计算是基于程序跑分数据的report进行基准评定
若后台数据库中没有相关report数据，则根据机器的基础数据进行模拟评定

## CPU基准模拟评定
根据单核心速度和总的核心数量进行核心评分计算，规则如下
```
double single = speed * 3.5;
int score = (int) ((single * 0.6) + (single * 4 * 0.3) + (single * coreNumber * 0.1));
if(coreNumber < 4) {
    score = (int) ((single * 0.8) + (single * 0.2 * coreNumber));
}
```

### GPU 暂无后台跑分规则

### Disk基准模拟评定
根据系统中的磁盘跑分中间数进行计算，若磁盘类型为SSD，则在中间数的基础上 + 500

### Memory基准模拟评定
根据当前内存的频率进行计算
```
score = (minSpeed / 2) + (100 * (size / 4))
```