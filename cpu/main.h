#ifndef __MAIN_H__
#define __MAIN_H__


#ifdef BUILD_DLL
    #define DLL_EXPORT __declspec(dllexport)
#else
    #define DLL_EXPORT __declspec(dllimport)
#endif

/**
 *  定义跑分方法
 *  参数1 ： 线程数
 *  参数2 ： 迭代次数
 *  返回值 ： 评分
 **/
int DLL_EXPORT benchmarks(int thread, int iterations);

#endif // __MAIN_H__
