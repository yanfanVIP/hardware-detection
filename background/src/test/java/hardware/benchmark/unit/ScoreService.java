package hardware.benchmark.unit;

import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import com.newegg.hardware.benchmark.AppMain;
import com.newegg.hardware.benchmark.service.CpuScoreService;
import com.newegg.hardware.benchmark.service.DiskScoreService;
import com.newegg.hardware.benchmark.service.GpuScoreService;
import com.newegg.hardware.benchmark.service.MemoryScoreService;

@SpringBootTest(classes=AppMain.class, webEnvironment=WebEnvironment.NONE)
public class ScoreService {

	@Autowired
	CpuScoreService cpu;
	
	@Autowired
	GpuScoreService gpu;
	
	@Autowired
	DiskScoreService disk;
	
	@Autowired
	MemoryScoreService mem;
	
	@Test
	public void initCPUData() throws IOException {
		System.out.println(cpu.getMeanScore());
	}
}
