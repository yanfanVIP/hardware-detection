package com.newegg.hardware.benchmark.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.newegg.hardware.benchmark.dao.HardmarkMapper;
import com.newegg.hardware.benchmark.dao.HistoryMapper;
import com.newegg.hardware.benchmark.model.ExpiredObject;
import com.newegg.hardware.benchmark.model.Hardmark;
import com.newegg.hardware.benchmark.model.History;
import com.newegg.hardware.benchmark.model.MarkCount;
import com.newegg.hardware.benchmark.model.Score;
import com.newegg.hardware.benchmark.service.MarkMean.Range;
import com.newegg.hardware.benchmark.util.JSONUtil;

@Service
public class MemoryScoreService implements HardwareServiceHander{
	static String TYPE = "MEMORY";
	@Autowired
	HardmarkMapper hardmarkMapper;
	@Autowired
	HistoryMapper historyMapper;
	
	ExpiredObject<Range> range;
	
	static String getName(JSONObject data) {
		String name = data.getString("PartNumber");
		if(name == null) { return null; }
		if(name.lastIndexOf("-") != -1) {
			return name.substring(0, name.lastIndexOf("-")).toLowerCase().trim();
		}
		return name.toLowerCase().trim();
	}
	
	public Hardmark getScoreByName(String name) {
		if(name == null) { return null; }
		Hardmark mark = hardmarkMapper.find(name, TYPE);
		if(mark == null && name.lastIndexOf("-") != -1) {
			mark = hardmarkMapper.find(name.substring(0, name.lastIndexOf("-")).toLowerCase().trim(), TYPE);
		}
		if(mark== null) {
			mark = getByHistory(name);
		}
		if(mark== null && name.lastIndexOf("-") != -1) {
			mark = getByHistory(name.substring(0, name.lastIndexOf("-")).toLowerCase().trim() + "%");
		}
		return mark;
	}
	
	@Override
	public Score getExistScore(JSONArray datas) {
		boolean simulate = false;
		int score = 1000000;
		boolean hasData = false;
		for (int i = 0; i < datas.size(); i++) {
			JSONObject data = datas.getJSONObject(i);
			String name = getName(data);
			if(name == null) { return null; }
			Hardmark mark = getScoreByName(name);
			if(mark == null || mark.getMark() == null) { continue; }
			if(score > mark.getMark()) {
				score = mark.getMark();
				simulate = mark.getSimulate();
				hasData = true;
			}
		}
		if(!hasData) { return null; }
		Score result = new Score();
		result.setScore(score);
		result.setMean(getMeanScore());
		result.setStar(getStar(result.getScore()));
		result.setSimulate(simulate);
		return result;
	}

	@Override
	public int getMeanScore() {
		if(range == null || range.isExpired()) {
			initRange();
		}
		return range.get().mean;
	}
	
	@Override
	public int getStar(int score) {
		if(range == null || range.isExpired()) {
			initRange();
		}
		Range r = range.get();
		int start = r.start;
		int end = r.end;
		if(score < start) { return 1; }
		if(score > end) { return 5; }
		return (int) Math.ceil((score - start) / Math.ceil((end - start) / 5));
	}

	private void initRange() {
		List<MarkCount> markcounts = hardmarkMapper.marks(TYPE);
		Range result = MarkMean.getMarkRange(markcounts);
		if(result == null) {
			Integer mean = hardmarkMapper.mean(TYPE);
			mean = mean == null ? 1500 : mean;
			result = new Range(mean - 1000, mean + 1000, mean);
		}
		range = new ExpiredObject<Range>(result, 24 * 60 * 60 * 1000);
	}
	
	@Override
	public int getVirtulScore(JSONArray datas, Integer defaultScore) {
		if(defaultScore != null && defaultScore > 0) { return defaultScore; }
		int minSpeed = 100000;
		int size = 0;
		for (int i = 0; i < datas.size(); i++) {
			JSONObject data = datas.getJSONObject(i);
			long capacity = data.getLongValue("Capacity");
			int speed = data.getIntValue("ConfiguredClockSpeed");
			size += (int)(capacity / 1024 / 1024);
			if(minSpeed > speed) {
				minSpeed = speed;
			}
		}
		int bei = size / 4;
		if(bei > 4) { bei = 4; }
		return (int)(minSpeed / 2) + (100 * bei);
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void insertHistory(JSONArray datas, Score score) {
		if(score == null || score.getScore() <= 0) { return; }
		JSONUtil.foreach(datas, (data, index)->{
			String name = getName(data);
			//将当前跑分的硬件信息入库
			History history = new History();
			history.setName(name);
			history.setType(TYPE);
			history.setManufacture(data.getString("Manufacturer"));
			history.setFamily(data.getInteger("SMBIOSMemoryType") + ":" + data.getInteger("TypeDetail") + ":" + data.getInteger("TotalWidth"));
			history.setModel(data.getInteger("Speed") + "");
			history.setFullname(data.getString("PartNumber"));
			history.setDriver(data.getString("Name"));
			history.setSerial(data.getString("SerialNumber"));
			history.setMark(score.getScore());
			historyMapper.insert(history);
		});
	}
	
	@Override
	public Hardmark getByHistory(String name) {
		History score = historyMapper.seachByName(name, TYPE);
		if(score == null) { return null; }
		Hardmark mark = new Hardmark();
		mark.setName(score.getName());
		mark.setMark(score.getMark());
		mark.setSimulate(false);
		mark.setType(TYPE);
		return mark;
	}
}
