package com.newegg.hardware.benchmark.dao;

import java.util.List;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import com.newegg.hardware.benchmark.model.Hardmark;
import com.newegg.hardware.benchmark.model.MarkCount;

@Mapper
public interface HardmarkMapper {

	@Select("select * from hardmark where name=#{name} and type=#{type}")
	public Hardmark find(String name, String type);
	
	@Select("select mark, count from hardmark where type=#{type} AND count > 0 order by mark desc")
	public List<MarkCount> marks(String type);
	
	@Select("select sum(mark * count) / sum(count) from hardmark where type=#{type}")
	public Integer mean(String type);
	
	@Insert("insert into hardmark(name, type, mark, count, simulate) values(#{name}, #{type}, #{mark}, #{count}, #{simulate}) "
			+ "ON DUPLICATE KEY UPDATE  mark=#{mark}, count=#{count}, simulate=#{simulate}")
    public int insert(Hardmark data);
}
