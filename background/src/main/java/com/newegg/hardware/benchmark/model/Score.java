package com.newegg.hardware.benchmark.model;

public class Score {
	//得分
	int score = 0;
	//平均分
	int mean = 0;
	//星级
	int star = 0;
	//是否是模拟分数
	boolean simulate;
	
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public int getMean() {
		return mean;
	}
	public void setMean(int mean) {
		this.mean = mean;
	}
	public boolean isSimulate() {
		return simulate;
	}
	public void setSimulate(boolean simulate) {
		this.simulate = simulate;
	}
	public int getStar() {
		return star;
	}
	public void setStar(int star) {
		this.star = star;
	}
}
