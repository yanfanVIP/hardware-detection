package com.newegg.hardware.benchmark.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.newegg.hardware.benchmark.model.History;


/**
CREATE TABLE `history` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL COMMENT '名称' COLLATE 'utf8mb4_general_ci',
	`type` VARCHAR(50) NOT NULL COMMENT '数据类型' COLLATE 'utf8mb4_general_ci',
	`manufacture` VARCHAR(50) NULL DEFAULT NULL COMMENT '厂家' COLLATE 'utf8mb4_general_ci',
	`family` VARCHAR(50) NULL DEFAULT NULL COMMENT '系列' COLLATE 'utf8mb4_general_ci',
	`model` VARCHAR(50) NULL DEFAULT NULL COMMENT '产品类型' COLLATE 'utf8mb4_general_ci',
	`fullname` VARCHAR(200) NOT NULL COMMENT '全称' COLLATE 'utf8mb4_general_ci',
	`driver` VARCHAR(200) NULL DEFAULT NULL COMMENT '驱动' COLLATE 'utf8mb4_general_ci',
	`serial` VARCHAR(200) NULL DEFAULT NULL COMMENT '设备ID' COLLATE 'utf8mb4_general_ci',
	`mark` INT(11) NOT NULL COMMENT '评分',
	PRIMARY KEY (`id`) USING BTREE
)
COMMENT='跑分历史记录表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=10000;
 */
@Mapper
public interface HistoryMapper {
	
	@Insert("insert into history(name, type, manufacture, family, model, fullname, driver, serial, mark) "
			+ "values(#{name}, #{type}, #{manufacture}, #{family}, #{model}, #{fullname}, #{driver}, #{serial}, #{mark})")
    public int insert(History data);

	@Select("select * from history where type=#{type} and (fullname like '${name}' or name=#{name}) limit 1")
	History seachByName(@Param("name") String name, @Param("type") String type);
}
