package com.newegg.hardware.benchmark.model;

public class HardwareScore extends Score{
	Score cpu;
	Score gpu;
	Score memory;
	Score disk;
	
	public Score getCpu() {
		return cpu;
	}
	public void setCpu(Score cpu) {
		this.cpu = cpu;
		this.score += this.cpu.getScore();
		this.mean += this.cpu.getMean();
		this.simulate = this.simulate || this.cpu.isSimulate();
	}
	
	public Score getGpu() {
		return gpu;
	}
	public void setGpu(Score gpu) {
		this.gpu = gpu;
		this.score += this.gpu.getScore();
		this.mean += this.gpu.getMean();
		this.simulate = this.simulate || this.gpu.isSimulate();
	}
	public Score getMemory() {
		return memory;
	}
	public void setMemory(Score memory) {
		this.memory = memory;
		this.score += this.memory.getScore();
		this.mean += this.memory.getMean();
		this.simulate = this.simulate || this.memory.isSimulate();
	}
	public Score getDisk() {
		return disk;
	}
	public void setDisk(Score disk) {
		this.disk = disk;
		this.score += this.disk.getScore();
		this.mean += this.disk.getMean();
		this.simulate = this.simulate || this.disk.isSimulate();
	}
	
	@Override
	public int getStar() {
		if(this.cpu == null || this.gpu == null || this.disk == null || this.memory == null) { return 0; }
		this.star = 0;
		this.star += this.cpu.star * 35 / 100;
		this.star += this.gpu.star * 35 / 100;
		this.star += this.disk.star * 10 / 100;
		this.star += this.memory.star * 15 / 100;
		return star;
	}
}
