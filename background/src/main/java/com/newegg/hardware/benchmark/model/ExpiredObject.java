package com.newegg.hardware.benchmark.model;

public class ExpiredObject<T> {
	
	T data;
	long timeoutdate;
	
	public ExpiredObject(T data, long timeout) {
		this.data = data;
		this.timeoutdate = System.currentTimeMillis() + timeout;
	}
	
	public T get() {
		if(System.currentTimeMillis() > this.timeoutdate) { return null; }
		return data;
	}
	
	public boolean isExpired() {
		return System.currentTimeMillis() > this.timeoutdate;
	}
}
