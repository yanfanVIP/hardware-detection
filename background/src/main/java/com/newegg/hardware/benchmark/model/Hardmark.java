package com.newegg.hardware.benchmark.model;

import java.io.Serializable;



/**
CREATE TABLE `hardmark` (
	`name` VARCHAR(50) NOT NULL COMMENT '名称' COLLATE 'utf8mb4_general_ci',
	`type` VARCHAR(50) NOT NULL COMMENT '类型' COLLATE 'utf8mb4_general_ci',
	`mark` INT(11) NOT NULL COMMENT '评分',
	`count` INT(11) NOT NULL DEFAULT '0' COMMENT '样品数量',
	`simulate` TINYINT(1) NOT NULL COMMENT '是否是模拟分',
	PRIMARY KEY (`name`, `type`) USING BTREE
)
COMMENT='硬件评分数据库'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB;
 */
public class Hardmark implements Serializable{
	private static final long serialVersionUID = -1731955186371523035L;
	/**
	 * 硬件名称
	 */
	String name;
	/**
	 * 硬件类型
	 */
	String type;
	/**
	 * 评分数据
	 */
	Integer mark;
	/**
	 * 样品数 
	 */
	Integer count;
	/**
	 * 是否为模拟数据
	 */
	Boolean simulate;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getMark() {
		return mark;
	}
	public void setMark(Integer mark) {
		this.mark = mark;
	}
	public Boolean getSimulate() {
		return simulate;
	}
	public void setSimulate(Boolean simulate) {
		this.simulate = simulate;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
}
