package com.newegg.hardware.benchmark.model;

import java.io.Serializable;

public class Itemmark implements Serializable{
	private static final long serialVersionUID = -4575091123150821918L;

	/**
	 * itemnumber
	 */
	String id;
	/**
	 * 硬件名称
	 */
	String name;
	/**
	 * 硬件类型
	 */
	String type;
	/**
	 * 评分数据
	 */
	Integer mark;
	/**
	 * 是否为模拟数据
	 */
	Boolean simulate;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getMark() {
		return mark;
	}
	public void setMark(Integer mark) {
		this.mark = mark;
	}
	public Boolean getSimulate() {
		return simulate;
	}
	public void setSimulate(Boolean simulate) {
		this.simulate = simulate;
	}
}
