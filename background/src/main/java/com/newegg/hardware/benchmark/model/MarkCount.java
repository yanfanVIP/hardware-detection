package com.newegg.hardware.benchmark.model;

public class MarkCount {

	int mark;
	int count;
	
	public MarkCount() {}
	
	public MarkCount(int mark, int count) {
		super();
		this.mark = mark;
		this.count = count;
	}

	public int getMark() {
		return mark;
	}
	public void setMark(int mark) {
		this.mark = mark;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
}
