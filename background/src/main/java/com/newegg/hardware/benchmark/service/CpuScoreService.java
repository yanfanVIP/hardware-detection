package com.newegg.hardware.benchmark.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.newegg.hardware.benchmark.dao.HardmarkMapper;
import com.newegg.hardware.benchmark.dao.HistoryMapper;
import com.newegg.hardware.benchmark.model.ExpiredObject;
import com.newegg.hardware.benchmark.model.Hardmark;
import com.newegg.hardware.benchmark.model.History;
import com.newegg.hardware.benchmark.model.MarkCount;
import com.newegg.hardware.benchmark.model.Score;
import com.newegg.hardware.benchmark.service.MarkMean.Range;

@Service
public class CpuScoreService implements HardwareServiceHander{
	static String TYPE = "CPU";
	@Autowired
	HardmarkMapper hardmarkMapper;
	@Autowired
	HistoryMapper historyMapper;
	
	ExpiredObject<Range> range;
	
	static String getName(JSONObject data) {
		String name = data.getString("Name");
		if(name == null) { return null; }
		name = name.toLowerCase().replace("(r)", "").replace("(tm)", "").replace("cpu ", "").trim();
		if(name.indexOf("@") != -1) {
			name = name.substring(0, name.indexOf("@")).trim();
		}
		return name;
	}
	
	@Transactional(rollbackFor=Exception.class)
	public Hardmark getScoreByName(String name) {
		if(name == null) { return null; }
		Hardmark mark = hardmarkMapper.find(name, TYPE);
		if(mark == null) {
			name = name.toLowerCase().replace("(r)", "").replace("(tm)", "").replace("cpu ", "").trim();
			mark = hardmarkMapper.find(name, TYPE);
		}
		if(mark == null) {
			mark = getByHistory(name);
		}
		if(mark == null) {
			name = "%" + name.replace(" ", "%") + "%";
			mark = getByHistory(name);
		}
		return mark;
	}
	
	@Override
	public Score getExistScore(JSONArray datas) {
		boolean simulate = datas.size() > 1;
		int score = 0;
		for (int i = 0; i < datas.size(); i++) {
			JSONObject data = datas.getJSONObject(i);
			String name = getName(data);
			if(name == null) { return null; }
			Hardmark mark = getScoreByName(name);
			if(mark == null || mark.getMark() == null) { return null; }
			simulate = simulate || mark.getSimulate();
			score += mark.getMark();
		}
		Score result = new Score();
		result.setScore(score);
		result.setMean(getMeanScore());
		result.setStar(getStar(result.getScore()));
		result.setSimulate(simulate);
		return result;
	}

	@Override
	public int getMeanScore() {
		if(range == null || range.isExpired()) {
			initRange();
		}
		return range.get().mean;
	}
	
	@Override
	public int getStar(int score) {
		if(range == null || range.isExpired()) {
			initRange();
		}
		Range r = range.get();
		int start = r.start;
		int end = r.end;
		if(score < start) { return 1; }
		if(score > end) { return 5; }
		return (int) Math.ceil((score - start) / Math.ceil((end - start) / 5));
	}
	
	private void initRange() {
		List<MarkCount> markcounts = hardmarkMapper.marks(TYPE);
		Range result = MarkMean.getMarkRange(markcounts);
		if(result == null) {
			Integer mean = hardmarkMapper.mean(TYPE);
			mean = mean == null ? 10000 : mean;
			result = new Range(mean - 5000, mean + 5000, mean);
		}
		range = new ExpiredObject<Range>(result, 24 * 60 * 60 * 1000);
	}
	
	@Override
	public int getVirtulScore(JSONArray datas, Integer defaultScore) {
		if(defaultScore != null && defaultScore > 0) { return defaultScore; }
		int s = 0;
		for (int i = 0; i < datas.size(); i++) {
			JSONObject data = datas.getJSONObject(i);
			int speed = data.getIntValue("MaxClockSpeed");
			int coreNumber = data.getIntValue("NumberOfCores");
			double single = speed * 3.5;
			int score = (int) ((single * 0.6) + (single * 4 * 0.3) + (single * coreNumber * 0.1));
			if(coreNumber < 4) {
				score = (int) ((single * 0.8) + (single * 0.2 * coreNumber));
			}
			s += score;
		}
		double specific = 1 - (datas.size() * 0.2);
		specific = specific < 0.5 ? 0.5 : specific;
		return (int) (s * specific);
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void insertHistory(JSONArray datas, Score score) {
		if(score == null || score.getScore() <= 0) { return; }
		for (int i = 0; i < datas.size(); i++) {
			JSONObject data = datas.getJSONObject(i);
			String name = getName(data);
			//将当前跑分的硬件信息入库
			History history = new History();
			history.setName(name);
			history.setType(TYPE);
			history.setManufacture(data.getString("Manufacturer"));
			history.setFamily(data.getInteger("Family") + "");
			history.setModel(data.getString("Description"));
			history.setFullname(data.getString("Name"));
			history.setDriver(data.getInteger("ProcessorType") + "");
			history.setSerial(data.getString("ProcessorId"));
			history.setMark(score.getScore());
			historyMapper.insert(history);
		}
	}

	@Override
	public Hardmark getByHistory(String name) {
		History score = historyMapper.seachByName(name, TYPE);
		if(score == null) { return null; }
		Hardmark mark = new Hardmark();
		mark.setName(score.getName());
		mark.setMark(score.getMark());
		mark.setSimulate(false);
		mark.setType(TYPE);
		return mark;
	}
}