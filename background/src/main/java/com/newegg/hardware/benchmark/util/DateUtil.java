package com.newegg.hardware.benchmark.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间帮助类
 * @author fy68
 */
public class DateUtil {
	
	public static enum Styles {
		TZ("yyyy-MM-dd'T'HH:mm:ss'Z'"),  
        YYYY_MM_DD_HH_MM_SS("yyyy-MM-dd HH:mm:ss"), 
        YYYY_MM_DD("yyyy-MM-dd"),
        YYYY_MM_DD_HH_MM_SS_SSS("yyyy-MM-dd HH:mm:ss.SSS"),
        
        YYYY_MM("yyyy-MM"), 
        YYYY_MM_EN("yyyy/MM"),  
        YYYY_MM_DD_EN("yyyy/MM/dd"),
        YYYY_MM_DD_HH_MM("yyyy-MM-dd HH:mm"),
        YYYY_MM_DD_HH_MM_EN("yyyy/MM/dd HH:mm"),  
        YYYY_MM_DD_HH_MM_SS_EN("yyyy/MM/dd HH:mm:ss"),  
          
        YYYY_MM_CN("yyyy年MM月"),  
        YYYY_MM_DD_CN("yyyy年MM月dd日"),  
        YYYY_MM_DD_HH_MM_CN("yyyy年MM月dd日 HH:mm"),  
        YYYY_MM_DD_HH_MM_SS_CN("yyyy年MM月dd日 HH:mm:ss"),  
          
        HH_MM("HH:mm"),  
        HH_MM_SS("HH:mm:ss"),  
          
        MM_DD("MM-dd"),  
        MM_DD_HH_MM("MM-dd HH:mm"),  
        MM_DD_HH_MM_SS("MM-dd HH:mm:ss"),  
          
        MM_DD_EN("MM/dd"),  
        MM_DD_HH_MM_EN("MM/dd HH:mm"),  
        MM_DD_HH_MM_SS_EN("MM/dd HH:mm:ss"),  
          
        MM_DD_CN("MM月dd日"),  
        MM_DD_HH_MM_CN("MM月dd日 HH:mm"),  
        MM_DD_HH_MM_SS_CN("MM月dd日 HH:mm:ss");  
		
		Styles(String value) { this.value = value; }
		
		private String value;  
        public String getValue() { return value; } 
	}

	@SuppressWarnings("deprecation")
	public static Date parse(String dateString) {
		Date date = null;
		if(date == null) {
			try { date = new Date(dateString); } catch (Exception e) { }
		}
		if(date == null) {
			try { date = Date.from(ZonedDateTime.parse(dateString).toInstant()); } catch (Exception e) {}
		}
		if(date == null) {
			for (Styles style : Styles.values()) {
				try { return parse(dateString, style.value); } catch (Exception e) {}
			}
		}
		return date;
	}
	
	public static Date parse(String dateString, String pattern) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		return dateFormat.parse(dateString);
	}
	
	public static String format(Date date, String style) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(style);
		return dateFormat.format(date);
	}
	
	public static String format(Date date, Styles style) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(style.value);
		return dateFormat.format(date);
	}
	
	public static String formatTime(Date date) {
		return format(date, Styles.YYYY_MM_DD_HH_MM_SS);
	}
	
	public static String formatDate(Date date) {
		return format(date, Styles.YYYY_MM_DD);
	}
	
	public static String getYestday(Styles style) {
		return getYestday(style.value);
	}
	
	public static String getYestday(String style) {
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, -1);
		return format(calendar.getTime(), style);
	}
	
	public static String getYestday(String date, String style) {
		try { return getYestday(parse(date, style), style); } catch (ParseException e) { e.printStackTrace(); }
		return date;
	}
	
	public static String getYestday(Date date, String style) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, -1);
		return format(calendar.getTime(), style);
	}
	
	public static void main(String[] args) throws ParseException {
		System.out.println(getYestday("20210201", "yyyyMMdd"));
	}
}