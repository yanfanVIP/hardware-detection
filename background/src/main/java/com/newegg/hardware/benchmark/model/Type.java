package com.newegg.hardware.benchmark.model;

public enum Type {
	CPU,
	GPU,
	MEMORY,
	DISK
}
