package com.newegg.hardware.benchmark.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import com.newegg.hardware.benchmark.model.Itemmark;


/**
CREATE TABLE `itemmark` (
	`id` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
	`name` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
	`type` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
	`mark` INT(11) NOT NULL,
	`simulate` TINYINT(1) NOT NULL,
	PRIMARY KEY (`id`) USING BTREE
)
COMMENT='关联商品的打分数据'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB;
 */
@Mapper
public interface ItemmarkMapper {
	
	@Insert("insert into itemmark(id, name, type, mark, simulate) values(#{id}, #{name}, #{type}, #{mark}, #{simulate})")
    public int insert(Itemmark data);

	@Select("select * from itemmark where id=#{id}")
	Itemmark get(@Param("id") String id);
}
