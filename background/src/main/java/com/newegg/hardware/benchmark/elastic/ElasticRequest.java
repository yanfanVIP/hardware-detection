package com.newegg.hardware.benchmark.elastic;

import java.util.Map;

public class ElasticRequest{
	String index;
	String key;
	Map<String, Object> data;
	int retry = 3;
	
	public ElasticRequest(String index, String key, Map<String, Object> data){
		this.index = index;
		this.key = key;
		this.data = data;
	}
	
	public String index() {
		return index;
	}
	public ElasticRequest setIndex(String index) {
		this.index = index;
		return this;
	}
	public String key() {
		return key;
	}
	public ElasticRequest setKey(String key) {
		this.key = key;
		return this;
	}
	public Map<String, Object> getData() {
		return data;
	}
	public ElasticRequest setData(Map<String, Object> data) {
		this.data = data;
		return this;
	}
	public int retry() {
		return retry;
	}
	public ElasticRequest setRetry(int retry) {
		this.retry = retry;
		return this;
	}
}
