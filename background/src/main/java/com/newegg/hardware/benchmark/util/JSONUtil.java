package com.newegg.hardware.benchmark.util;

import java.util.function.BiConsumer;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class JSONUtil {
	public static void foreach(JSONArray arrays, BiConsumer<JSONObject, Integer> accept) {
		for (int i = arrays.size() - 1; i >= 0; i--) {
			JSONObject object = arrays.getJSONObject(i);
			accept.accept(object, i);
		}
	}
}
