package com.newegg.hardware.benchmark.model;

public class BaseAPI{

	public Response<Void> SUCCESS() {
		Response<Void> response = new Response<Void>();
		response.setCode(0);
		return response;
	}
	
	public <T> Response<T> SUCCESS(T data) {
		Response<T> response = new Response<T>();
		response.setCode(0);
		response.setData(data);
		return response;
	}
	
	public <T> Response<T> SUCCESS(T data, String message) {
		Response<T> response = new Response<T>();
		response.setCode(0);
		response.setMessage(message);
		response.setData(data);
		return response;
	}
	
	public <T> Response<T> FAIL(int code, String message) {
		Response<T> response = new Response<T>();
		response.setCode(code);
		response.setMessage(message);
		return response;
	}
}
