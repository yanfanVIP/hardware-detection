package com.newegg.hardware.benchmark.service;

import com.alibaba.fastjson.JSONArray;
import com.newegg.hardware.benchmark.model.Hardmark;
import com.newegg.hardware.benchmark.model.Score;

public interface HardwareServiceHander {

	default public Score getScore(JSONArray datas, Integer defaultScore) {
		Score score = getExistScore(datas);
		if(score != null) {
			if(defaultScore != null && defaultScore > 0){
				score.setScore(defaultScore);
				insertHistory(datas, score);
			}
			return score;
		}
		score = new Score();
		score.setMean(getMeanScore());
		score.setScore(getVirtulScore(datas, defaultScore));
		score.setStar(6);
		score.setSimulate(true);
		return score;
	}
	/**
	 * 根据硬件名称获取跑分数据
	 */
	Hardmark getScoreByName(String name);
	/**
	 * 获取当前硬件已经存在的评分数据
	 * 若不存在数据，则调用虚拟评分数据
	 */
	Score getExistScore(JSONArray datas);
	/**
	 * 获取当前硬件的平均分
	 */
	int getMeanScore();
	/**
	 * 获取当前硬件的星级（10分制）
	 */
	int getStar(int score);
	/**
	 * 根据硬件规格，计算虚拟评分
	 */
	int getVirtulScore(JSONArray datas, Integer defaultScore);
	/**
	 * 用户打开应用后或者跑分后，将用户的跑分入库到数据库中
	 */
	void insertHistory(JSONArray datas, Score score);
	/**
	 * 根据名称从history中查询
	 */
	Hardmark getByHistory(String name);
}
