package com.newegg.hardware.benchmark.model;

import com.alibaba.fastjson.JSONObject;

public class ItemSpec {
	
	String name;
	Type type;
	
	JSONObject spec = new JSONObject();
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public void setSpec(JSONObject spec) {
		this.spec = spec;
	}

	public ItemSpec(Type type){
		this.type = type;
	}

	public Type getType() {
		return type;
	}

	public JSONObject getSpec() {
		return spec;
	}

	public void append(String key, Object value) {
		spec.put(key, value);
	}
}
