package com.newegg.hardware.benchmark.api;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.alibaba.fastjson.JSONObject;
import com.newegg.hardware.benchmark.model.BaseAPI;
import com.newegg.hardware.benchmark.model.HardwareScore;
import com.newegg.hardware.benchmark.model.Response;
import com.newegg.hardware.benchmark.model.Score;
import com.newegg.hardware.benchmark.model.Type;
import com.newegg.hardware.benchmark.service.ScoreService;

@RestController
@RequestMapping("/api/hardware")
public class HardwareAPI extends BaseAPI{
	@Autowired
	ScoreService scoreService;
	
	@Value("${hardware.version}")
	String version;
	@Value("${hardware.version.url}")
	String versionurl;
	
	@GetMapping("/findByName")
    public Response<Score> findByName(@RequestParam Type type, @RequestParam String name, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return SUCCESS(scoreService.findByName(type, name));
    }
	
	@GetMapping("/findByItem")
    public Response<Score> findByItem(@RequestParam String itemnumber, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return SUCCESS(scoreService.findByItem(itemnumber));
    }
	
    @PostMapping("/score")
    public Response<HardwareScore> score(@RequestBody JSONObject json, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return SUCCESS(scoreService.getScore(json));
    }
    
    @GetMapping("/version")
    public Response<Map<String, String>> version() {
    	Map<String, String> map = new HashMap<String, String>();
    	map.put("version", version);
    	map.put("url", versionurl);
		return SUCCESS(map);
    }
}
