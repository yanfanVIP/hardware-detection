#include <stdio.h>
#include <time.h>
#include <CL/cl.h>

#include "main.h"

#define MAX_SOURCE_SIZE (0x100000)

struct Result SUCCESS(int runtime, int max_compute_units, int score){
	struct Result result = {runtime, max_compute_units, score, 0};
	return result;
};

struct Result FAILURE(int state){
	struct Result result = {0, 0, 0, state};
	return result;
};

struct Result DLL_EXPORT benchmark(char* c_kernel_source){
    cl_uint deviceNum;
    cl_platform_id platform = NULL;
    cl_device_id device;
	cl_int status;

	status = clGetPlatformIDs(1, &platform, NULL);
	if (status != CL_SUCCESS){
        printf("Error: can not get platforms!\n");
        return FAILURE(status);
	}
    status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 0, NULL, &deviceNum);
    if (status != CL_SUCCESS){
        printf("Error: can not find gpu device\n");
        return FAILURE(status);
    }
    if (deviceNum == 0){
        printf("WARNING: no gpu device available\n");
        return FAILURE(-1);
    }

    status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, deviceNum, &device, NULL);
    if (status != CL_SUCCESS){
        printf("Error: can not get default gpu device\n");
        return FAILURE(status);
    }

    cl_context context = clCreateContext(NULL, 1, &device, NULL, NULL, &status);
    if (status != CL_SUCCESS){
        printf("Error: can not create opencl context!\n");
        return FAILURE(status);
    }

	cl_command_queue commandQueue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &status);
	if (status != CL_SUCCESS){
        printf("Error: can not create opencl command queue!\n");
        return FAILURE(status);
    }

	cl_program program = clCreateProgramWithSource(context, 1, (const char **)&c_kernel_source, NULL, &status);
	if (status != CL_SUCCESS){
        printf("Error: can not create program with opencl source!\n");
        return FAILURE(status);
    }

	status = clBuildProgram(program, 1, &device, NULL, NULL, NULL);
	if (status != CL_SUCCESS){
        printf("Error: can not build program with opencl source\n");
        return FAILURE(status);
    }
	cl_kernel kernel = clCreateKernel(program, "benchmark", &status);
	clFinish(commandQueue);

	cl_uint max_compute_units;
    cl_uint max_work_item_dim;
    cl_uint uconstant_args = 0;
    size_t work_item_sizes[3];
    size_t  max_work_group_size;
    size_t  max_work_item_size;
    size_t  max_kernel_work_group_size;
    cl_ulong uconstant_buffer_size = 0;
    char pform_vendor[40];

    clGetPlatformInfo(platform, CL_PLATFORM_VENDOR, sizeof(pform_vendor), &pform_vendor, NULL);
    clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(cl_uint), &max_work_item_dim, NULL);
    clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(work_item_sizes), (void *)work_item_sizes, NULL);
    clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), &max_work_group_size, NULL);
    clGetDeviceInfo(device, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &max_compute_units, NULL);
    clGetDeviceInfo(device, CL_DEVICE_MAX_CONSTANT_ARGS, sizeof(cl_uint), (void *)&uconstant_args, NULL);
    clGetDeviceInfo(device, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, sizeof(cl_ulong), (void *)&uconstant_buffer_size, NULL);
    clGetKernelWorkGroupInfo(kernel, device, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(size_t), &max_kernel_work_group_size, NULL);

    printf("gpu device nums             : %d\n", deviceNum);
    printf("vendor                      : %s\n", pform_vendor);
    printf("Max work-item dimensions    : %d\n", max_work_item_dim);
	printf("Max work-item sizes         : %d %d %d\n", work_item_sizes[0], work_item_sizes[1], work_item_sizes[2]);
    printf("Max work-group size         : %d\n", max_work_group_size);
    printf("Max comput_uint             : %u\n", max_compute_units);
    printf("Max constant_args           : %u\n", uconstant_args);
    printf("Max constant_buffer_size    : %u\n", uconstant_buffer_size);
    printf("Max preferred group size    : %u\n", max_kernel_work_group_size);


	float in = 1.1;
	float out = 0.0;
	cl_mem inputBuffer = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(float), NULL, NULL);
	cl_mem outputBuffer = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(float), NULL, NULL);
    clEnqueueWriteBuffer(commandQueue, inputBuffer, CL_TRUE, 0, sizeof(float), &in, 0, NULL, NULL);
	clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&inputBuffer);
	clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&outputBuffer);
	cl_event event;
	size_t local_work_size[1] = {max_work_group_size / 4};
	size_t global_work_size[1] = {max_work_group_size};
	status = clEnqueueNDRangeKernel(commandQueue, kernel, 1, NULL, global_work_size, local_work_size, 0, NULL, &event);
	if (status != CL_SUCCESS){
        printf("Error: can not create opencl range kernel!\n");
        return FAILURE(status);
    }
	clWaitForEvents(1, &event);
	status = clEnqueueReadBuffer(commandQueue, outputBuffer, CL_TRUE, 0, sizeof(float), &out, 0, NULL, NULL);
	if (status != CL_SUCCESS){
        printf("Error: can not read data for opencl!\n");
        return FAILURE(status);
    }
	clFlush(commandQueue);

	cl_ulong time_start, time_end;
	clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
	clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);
    double runtime = (time_end - time_start) / 1000000.0;
    double score = 5000.0 / runtime * (max_compute_units * max_kernel_work_group_size * 0.5);

  	printf("result                  : %f\n", out);
  	printf("runtime                 : %f ms\n", runtime);
    printf("score                   : %d\n", (int) score);

	clReleaseEvent(event);
	clReleaseKernel(kernel);
	clReleaseProgram(program);
	clReleaseMemObject(inputBuffer);
	clReleaseMemObject(outputBuffer);
	clReleaseCommandQueue(commandQueue);
	clReleaseContext(context);
    return SUCCESS((int) runtime,(int) max_compute_units * max_kernel_work_group_size, (int) score);
};