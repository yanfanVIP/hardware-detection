#ifndef __MAIN_H__
#define __MAIN_H__

#ifdef BUILD_DLL
    #define DLL_EXPORT __declspec(dllexport)
#else
    #define DLL_EXPORT __declspec(dllimport)
#endif

struct Result{
    int runtime;
    int max_compute_units;
    int score;
    int state;
};

struct Result DLL_EXPORT benchmark(char* c_kernel_source);

#endif // __MAIN_H__
