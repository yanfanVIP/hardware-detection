kernel void benchmark(global float* in, global float* out){
    int num = get_global_id(0);
    out[num] = 2.1;
    for(int i=0; i < 256; i++){
        for(int j=i; j < 256; j++){
	        for(int k=i; k < 256; k++){
            	out[num] = (in[num] + out[num]) / (in[num] - out[num]) + (in[num] + out[num]);
            }
        }
    }
}