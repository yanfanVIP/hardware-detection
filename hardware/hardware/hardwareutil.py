import wmi
import winreg
import pythoncom
from hardware.edid import edid
import logging


class HardwareUtil:
    def __init__(self):
        pythoncom.CoInitialize()
        self.connect = wmi.WMI()

    def __del__(self):
        try:
            pythoncom.CoUninitialize()
        except Exception as e:
            print(e)
            logging.error(e)

    # 获取系统信息
    def system(self):
        _d = self.connect.Win32_ComputerSystem()[0]
        _r = {}
        for _k in _d.properties:
            _v = _d.__getattr__(_k)
            _r[_k] = _v
        _r['TYPE'] = 'SYSTEM'
        if 'SystemFamily' in _r:
            _r['name'] = "{} {}".format(_r['Manufacturer'], _r['SystemFamily'])
        else:
            _r['name'] = str(_r['SystemType'])
        return _r

    # 获取主板信息
    def baseboard(self):
        _d = self.connect.Win32_BaseBoard()[0]
        _r = {}
        for _k in _d.properties:
            _v = _d.__getattr__(_k)
            _r[_k] = _v
        _r['TYPE'] = 'BASEBOARD'
        _r['name'] = "{} {}".format(_r['Manufacturer'], _r['Product'])
        return _r

    # 获取主板信息
    def bios(self):
        _d = self.connect.Win32_BIOS()[0]
        _r = {}
        for _k in _d.properties:
            _v = _d.__getattr__(_k)
            _r[_k] = _v
        _r['TYPE'] = 'BIOS'
        _r['name'] = str(_r['SMBIOSBIOSVersion'])
        return _r

    # 获取CPU列表
    def cpu(self):
        _rs = []
        _ds = self.connect.Win32_Processor()
        for _d in _ds:
            _r = {}
            for _k in _d.properties:
                _v = _d.__getattr__(_k)
                _r[_k] = _v
            _r['TYPE'] = 'CPU'
            _r['name'] = str(_r['Name'])
            _rs.append(_r)
        return _rs

    # 获取内存列表
    def memory(self):
        _rs = []
        _ds = self.connect.Win32_PhysicalMemory()
        for _d in _ds:
            _r = {}
            for _k in _d.properties:
                _v = _d.__getattr__(_k)
                _r[_k] = _v
            _r['TYPE'] = 'MEMORY'
            _r['name'] = "{} {} {}MHz ({})".format(_r['Manufacturer'], self.parseMemoryType(_r['SMBIOSMemoryType']), _r['Speed'], self.parseBits(_r['Capacity']))
            _rs.append(_r)
        return _rs

    # 获取硬盘
    def disk(self):
        _rs = []
        _ds = self.connect.Win32_DiskDrive()
        for _d in _ds:
            mainDisk = False
            for _partition in _d.associators("Win32_DiskDriveToDiskPartition"):
                if _partition.BootPartition:
                    mainDisk = True
            _r = {}
            for _k in _d.properties:
                _v = _d.__getattr__(_k)
                _r[_k] = _v
            _r['BootPartition'] = mainDisk
            _r['TYPE'] = 'DISK'
            _r['name'] = "{} ({})".format(_r['Model'], self.parseBits(_r['Size']))
            _rs.append(_r)
        return _rs

    # 获取显卡信息
    def gpu(self):
        _rs = []
        _ds = self.connect.Win32_VideoController(Availability=3)
        for _d in _ds:
            if _d.VideoProcessor is None:
                continue
            _r = {}
            for _k in _d.properties:
                _v = _d.__getattr__(_k)
                _r[_k] = _v
            _r['TYPE'] = 'GraphicsCard'
            _r['name'] = "{} {}".format(_r['Name'], self.parseBits(_r['AdapterRAM']))
            _rs.append(_r)
        return _rs

    # 获取显示器信息
    def display(self):
        displays = []
        for d in self.connect.Win32_PnPEntity(PNPClass="Monitor"):
            try:
                device_opend = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, "SYSTEM\\ControlSet001\\Enum\\" + d.PNPDeviceID + "\\Device Parameters")
                edid_bytes = winreg.QueryValueEx(device_opend, "EDID")
                display = edid(edid_bytes[0])
                _r = display.__dict__
                _r['TYPE'] = 'DISPLAY'
                if 'name' in _r and 'serial' in _r:
                    _r['name'] = "{} ({})".format(_r['name'], _r['serial'])
                elif 'name' in _r:
                    _r['name'] = str(_r['name'])
                elif 'serial' in _r:
                    _r['name'] = str(_r['serial'])
                else:
                    _r['name'] = 'Default Display'
                displays.append(_r)
            except Exception as e1:
                displays.append({'TYPE': 'DISPLAY', 'name': d.Name})
                logging.error(e1)
                continue
        return displays

    # 获取声音设备信息
    def sound(self):
        sounds = []
        for sound in self.connect.Win32_SoundDevice():
            if sound.Manufacturer != "(Generic USB Audio)":
                _r = {}
                for _k in sound.properties:
                    _v = sound.__getattr__(_k)
                    _r[_k] = _v
                _r['TYPE'] = 'SOUND'
                _r['name'] = str(_r['Name'])
                sounds.append(_r)
        return sounds

    # 获取键盘信息
    def keyboard(self):
        keyboard = self.connect.Win32_PNPEntity(PNPClass="Keyboard")
        if len(keyboard) > 0:
            _r = {}
            for _k in keyboard[0].properties:
                _v = keyboard[0].__getattr__(_k)
                _r[_k] = _v
            _r['TYPE'] = 'KEYBOARD'
            _r['name'] = str(_r['Name'])
            return _r
        return None

    # 获取鼠标信息
    def mouse(self):
        mouse = self.connect.Win32_PNPEntity(PNPClass="Mouse")
        if len(mouse) > 0:
            _r = {}
            for _k in mouse[0].properties:
                _v = mouse[0].__getattr__(_k)
                _r[_k] = _v
            _r['TYPE'] = 'MOUSE'
            _r['name'] = str(_r['Name'])
            return _r
        return None

    # 获取网卡信息
    def network(self):
        networks = []
        for network in self.connect.Win32_PNPEntity(PNPClass="Net"):
            if network.Manufacturer != 'Microsoft':
                _r = {}
                for _k in network.properties:
                    _v = network.__getattr__(_k)
                    _r[_k] = _v
                _r['TYPE'] = 'NETWORK'
                _r['name'] = str(_r['Name'])
                networks.append(_r)
        return networks

    # 获取播放设备
    def media(self):
        _rs = []
        _ds = self.connect.Win32_PNPEntity(PNPClass="MEDIA", Service="usbaudio")
        for _d in _ds:
            _r = {}
            for _k in _d.properties:
                _v = _d.__getattr__(_k)
                _r[_k] = _v
            _r['TYPE'] = 'MEDIA'
            _r['name'] = str(_r['Name'])
            _rs.append(_r)
        return _rs

    # 获取Windows激活码
    def productkey(self):
        _r = {'TYPE': 'PRODUCTKEY'}
        try:
            platform = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\SoftwareProtectionPlatform")
            productKey = winreg.QueryValueEx(platform, "BackupProductKeyDefault")
            _r['name'] = productKey[0]
        except Exception as e1:
            logging.error(e1)
            _r['name'] = 'NONE'
        return _r

    # 格式化内存版本号
    def parseMemoryType(self, type: int):
        types = {
            0x01: "Other",
            0x03: "DRAM",
            0x04: "EDRAM",
            0x05: "VRAM",
            0x06: "SRAM",
            0x07: "RAM",
            0x08: "ROM",
            0x09: "FLASH",
            0x0A: "EEPROM",
            0x0B: "FEPROM",
            0x0C: "EPROM",
            0x0D: "CDRAM",
            0x0E: "3DRAM",
            0x0F: "SDRAM",
            0x10: "SGRAM",
            0x11: "RDRAM",
            0x12: "DDR",
            0x13: "DDR2",
            0x14: "DDR2 FB-DIMM",
            0x18: "DDR3",
            0x19: "FBD2",
            0x1A: "DDR4",
            0x1B: "LPDDR",
            0x1C: "LPDDR2",
            0x1D: "LPDDR3",
            0x1E: "LPDDR4",
            0x1F: "Logical non-volatile device",
        }
        return types.get(type, None)

    def parseBits(self, capacity):
        if capacity is None:
            return "Zero bit"
        capacity = int(capacity)
        exts = {
            0: "bit",
            1: "KB",
            2: "MB",
            3: "GB",
            4: "TB"
        }
        i = 0
        while capacity >= 1024:
            i = i + 1
            capacity = capacity / 1024
        return str(round(capacity)) + "" + exts.get(i, "GB")


if __name__ == "__main__":
    print(HardwareUtil().gpu())