from hardware.hardwareutil import HardwareUtil
from concurrent.futures import ThreadPoolExecutor
import logging


def getAll():
    call_names = [
        'system',
        'baseboard',
        'bios',
        'cpu',
        'memory',
        'disk',
        'gpu',
        'display',
        'sound',
        'keyboard',
        'mouse',
        'network',
        'media',
        'productkey'
    ]

    results = {}

    def call(name):
        try:
            data = getattr(HardwareUtil(), name, None)()
            results[name] = data
        except Exception as e:
            results[name] = None
            logging.error(e)

    executor = ThreadPoolExecutor(max_workers=len(call_names))
    for result in executor.map(call, call_names, timeout=15):
        result
    return results


if __name__ == "__main__":
    hardware = getAll()
    text = ''
    for name in hardware:
        text = text + name + '\r\n'
        datas = hardware[name]
        if isinstance(datas, list):
            for item in datas:
                text = text + '  |--->' + item['name'] + '\r\n'
        else:
            text = text + '  |--->' + datas['name'] + '\r\n'
    print(text)
