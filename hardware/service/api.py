import json
import requests

REQUEST_URL = "https://ec-apis.newegg.com"
# REQUEST_URL = "http://localhost:8906"
HEADER = {
    'Content-Type': 'application/json; charset=utf-8',
    'Accept': 'application/json; charset=utf-8',
    'Authorization': 'bearer zp0DvoEhJIkdNUKsrp82Zx4UlDZq3IMu69gPF1Np'
}


def getVersion():
    response = None
    try:
        response = requests.get(REQUEST_URL + '/api/hardware/version', None, headers=HEADER)
        if response.status_code == 200:
            response = json.loads(response.text.encode())['data']
    finally:
        return response


def getHardwareScore(hardwares=[], score={}):
    request = {}
    for item in hardwares:
        request.update({item: hardwares[item]})
    request['cpu_score'] = score['cpu']['score']
    request['gpu_score'] = score['gpu']['score']
    request['disk_score'] = score['disk']['score']
    request['memory_score'] = score['memory']['score']
    request = json.dumps(request)
    response = None
    try:
        response = requests.post(REQUEST_URL + '/api/hardware/score', timeout=5, data=request, headers=HEADER)
        if response.status_code == 200:
            response = json.loads(response.text.encode())['data']
    finally:
        return response


def report(benchmark={}):
    response = None
    try:
        response = requests.post(REQUEST_URL + '/api/hardware/report', data=benchmark, headers=HEADER)
        if response.status_code == 200:
            response = json.loads(response.text.encode())['data']
    finally:
        return response


if __name__ == "__main__":
    version = getVersion()
    print(version)
