# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['app.py'],
             pathex=['F:\\workspaces\\python\\hardware_monitor'],
             binaries=None,
             datas=[
				('version.txt', '.'),
                ('assets', 'assets'),
                ('benchmarks/coremark.dll', '.'),
                ('benchmarks/clbenchmark.dll', '.'),
                ('benchmarks/memory.dll', '.'),
                ('benchmarks/libgcc_s_seh-1.dll', '.'),
                ('benchmarks/libstdc++-6.dll', '.'),
                ('benchmarks/libwinpthread-1.dll', '.')
             ],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None,
             excludes=None,
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)

pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='ENIAC.ESCORE',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False,
          icon='assets\\img\\icon.ico',
          version='file_version_info.txt')
