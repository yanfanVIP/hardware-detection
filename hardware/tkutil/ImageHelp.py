import sys
from PIL import Image, ImageTk
from tkinter import filedialog
from ctypes import *


class ImageHelp:
    icons = {}

    @staticmethod
    def getIcons(icon, size=None):
        base_path = ''
        if getattr(sys, 'frozen', False):
            base_path = sys._MEIPASS + '/'
        if ImageHelp.icons.get(icon) is None:
            img = Image.open(base_path + 'assets/img/' + icon + '.png')
            if size is not None:
                img = img.resize(size)
            ImageHelp.icons.setdefault(icon, ImageTk.PhotoImage(img))
        return ImageHelp.icons.get(icon)

    @staticmethod
    def capture():
        import win32gui, win32ui, win32con
        file_path = filedialog.asksaveasfilename(title='save image', filetypes=[('Image Files', '.bmp')], initialfile='ENIAC.bmp')
        if file_path is None:
            return
        if file_path == '':
            return
        hwnd = windll.user32.GetForegroundWindow()
        left, top, right, bot = win32gui.GetWindowRect(hwnd)
        width = right - left
        height = bot - top
        hWndDC = win32gui.GetWindowDC(hwnd)
        mfcDC = win32ui.CreateDCFromHandle(hWndDC)
        saveDC = mfcDC.CreateCompatibleDC()
        saveBitMap = win32ui.CreateBitmap()
        saveBitMap.CreateCompatibleBitmap(mfcDC, width, height)
        saveDC.SelectObject(saveBitMap)
        saveDC.BitBlt((0, 0), (width, height), mfcDC, (0, 0), win32con.SRCCOPY)
        saveBitMap.SaveBitmapFile(saveDC, file_path)