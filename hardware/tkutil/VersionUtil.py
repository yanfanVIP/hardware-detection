import sys


def getVersion():
    base_path = ''
    if getattr(sys, 'frozen', False):
        base_path = sys._MEIPASS + '/'
    with open(base_path + "version.txt", "r") as f:
        return f.read()


def compare(v1, v2):
    l_1 = v1.split('.')
    l_2 = v2.split('.')
    c = 0
    while True:
        if c == len(l_1) and c == len(l_2):
            return 0
        if len(l_1) == c:
            l_1.append(0)
        if len(l_2) == c:
            l_2.append(0)
        if int(l_1[c]) > int(l_2[c]):
            return 1
        elif int(l_1[c]) < int(l_2[c]):
            return -1
        c += 1


if __name__ == "__main__":
    print(compare("8.4.10", "8.4.9.3"))