#ifndef __MAIN_H__
#define __MAIN_H__


#ifdef BUILD_DLL
    #define DLL_EXPORT __declspec(dllexport)
#else
    #define DLL_EXPORT __declspec(dllimport)
#endif

/**
 *  定义跑分方法
 *  参数1 ： 窗口宽度
 *  参数2 ： 窗口高度
 *  参数3 ： 是否全屏
 *  参数4 ： 运行时间
 *  返回值 ： 评分
 **/

extern "C"
{
    int DLL_EXPORT benchmarks(int width, int height, int fullscreen, int time);
    int DLL_EXPORT benchmarks_assest(int width, int height, int fullscreen, int time, char* assest);
}
#endif // __MAIN_H__
